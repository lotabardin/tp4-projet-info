import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.FileWriter;
import java.io.IOException;

public class Simulation {
    private Grid grid;
    private List<Agent> agents;
    private Random random = new Random();
    private int day = 0;
    private final int width, height, totalDays;

    /**
     * Constructor for the Simulation class
     * @param numAgents The number of agents in the simulation
     * @param gridWidth The width of the grid
     * @param gridHeight The height of the grid
     * @param totalDays The total number of days for the simulation
     * @param number_infected The number of agents initially infected
     */
    public Simulation(int numAgents, int gridWidth, int gridHeight, int totalDays, int number_infected) {
        this.width = gridWidth;
        this.height = gridHeight;
        this.totalDays = totalDays;
        this.grid = new Grid(gridWidth, gridHeight);
        this.agents = new ArrayList<>(numAgents);
        initializeAgents(numAgents, number_infected);
    }

    /**
     * Initialize the agents in the simulation
     * @param numAgents The number of agents in the simulation
     * @param number_infected The number of agents initially infected
     */
    private void initializeAgents(int numAgents, int number_infected) {
        List<Agent> allAgents = new ArrayList<>();
    
        for (int i = 0; i < numAgents; i++) {
            int dE = (int) negExp(5); 
            int dI = (int) negExp(7); 
            int dR = (int) negExp(10); 
    
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            
            Agent.Status initialStatus = Agent.Status.S;
            if (i < number_infected) {
                initialStatus = Agent.Status.I;
            }
            Agent agent = new Agent(initialStatus, dE, dI, dR, x, y);
            allAgents.add(agent);
        }
    
        for (Agent agent : allAgents) {
            agents.add(agent);
            grid.addAgent(agent, agent.getX(), agent.getY());
        }
    }    

    /**
     * Run the simulation for the specified number of days
     * @param filePath The path to the CSV file to write the results
     */
    public void run(String filePath) {
        createCSV(filePath);
        appendDayToCSV(filePath, 0);
        for (day = 0; day < totalDays; day++) {
            java.util.Collections.shuffle(agents, random);
            for (Agent agent : agents) {
                updateAgentStatus(agent);
                moveAgentRandomly(agent);
            }
            appendDayToCSV(filePath, day);
        }
    }

    /**
     * Update the status of an agent based on the time spent in the current status
     * @param agent The agent to update
     */
    private void updateAgentStatus(Agent agent) {
        agent.update();
    }

    /**
     * Move an agent to a new random location on the grid
     * @param agent The agent to move
     */
    private void moveAgentRandomly(Agent agent) {
        int oldX = agent.getX();
        int oldY = agent.getY();
        int newX = random.nextInt(width);
        int newY = random.nextInt(height);

        if (agent.getStatus() == Agent.Status.I) {
            for (Agent otherAgent : agents) {
                if (otherAgent.getStatus() == Agent.Status.S && otherAgent.getX() == newX && otherAgent.getY() == newY) {
                    otherAgent.setStatus(Agent.Status.E);
                }
            }
        }

        grid.moveAgent(agent, oldX, oldY, newX, newY);
    }
    
    /**
     * Generate a random number from an exponential distribution
     * @param mean The mean of the distribution
     * @return A random number from the exponential distribution
     */
    private double negExp(double mean) {
        return -mean * Math.log(1 - random.nextDouble());
    }

    /**
     * Create a CSV file with the header row
     * @param filePath The path to the CSV file
     */
    public void createCSV(String filePath) {
        try (FileWriter writer = new FileWriter(filePath)) {
            writer.append("Day,S,E,I,R\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Append the agent counts for the current day to the CSV file
     * @param filePath The path to the CSV file
     * @param day The current day of the simulation
     */
    public void appendDayToCSV(String filePath, int day) {
        try (FileWriter writer = new FileWriter(filePath, true)) {
            int[] counts = getAgentCountsForDay();
            writer.append(day + "," + counts[0] + "," + counts[1] + "," + counts[2] + "," + counts[3] + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
    
    /**
     * Get the counts of agents in each status for the current day
     * @return An array of counts for each status (S, E, I, R)
     */
    private int[] getAgentCountsForDay() {
        int[] counts = new int[4];
        for (Agent agent : agents) {
            switch (agent.getStatus()) {
                case S:
                    counts[0]++;
                    break;
                case E:
                    counts[1]++;
                    break;
                case I:
                    counts[2]++;
                    break;
                case R:
                    counts[3]++;
                    break;
            }
        }
        return counts;
    }

}