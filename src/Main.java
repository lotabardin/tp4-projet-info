public class Main {
    public static void main(String[] args) {
        int numAgents = 20000;
        int gridWidth = 300;
        int gridHeight = 300;
        int totalDays = 730;
        int number_infected = 20;

        /**
         * Run the simulation 100 times with a reset of the parameters each time
         */
        for (int i = 1; i <= 100; i++) {
            Simulation simulation = new Simulation(numAgents, gridWidth, gridHeight, totalDays, number_infected);

            String fileName = "Test " + i + ".csv";
            simulation.run("C:\\Users\\loict\\Documents 2\\VSCode\\Projet Info\\TP4\\csv\\" + fileName);

            numAgents = 20000;
            gridWidth = 300;
            gridHeight = 300;
            totalDays = 730;
            number_infected = 20;
        }

        System.out.println("Simulation completed.");
    }
}