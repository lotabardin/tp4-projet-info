public class Agent {
    enum Status {S, E, I, R} // S: Susceptible, E: Exposed, I: Infected, R: Recovered
    private Status status; // Current status of the agent
    private int timeInStatus; // Time spent in the current status
    private final int dE, dI, dR; // Durations for E, I, R statuses.
    private int x, y; // Agent's position on the grid

    /**
     * Constructor for the Agent class
     * @param status
     * @param dE
     * @param dI
     * @param dR
     * @param x
     * @param y
     */
    public Agent(Status status, int dE, int dI, int dR, int x, int y) {
        this.status = status;
        this.dE = dE;
        this.dI = dI;
        this.dR = dR;
        this.timeInStatus = 0;
        this.x = x;
        this.y = y;
    }

    /**
     * Update the agent's status based on the time spent in the current status
     */
    public void update() {
        // Increment the time the agent has spent in the current status
        timeInStatus++;

        switch (status) {
            case E:
                if (timeInStatus > dE) {
                    status = Status.I;
                    timeInStatus = 0; // Reset time in status for the infected state
                }
                break;
            case I:
                if (timeInStatus > dI) {
                    status = Status.R;
                    timeInStatus = 0; // Reset time in status for the recovered state
                }
                break;
            case R:
                if (timeInStatus > dR) {
                    status = Status.S;
                    timeInStatus = 0; // Reset time in status for the susceptible state
                }
                break;
            default:
                // No action needed for S status in this context
                break;
        }
    }

    /**
     * Getter and Setter methods
     */
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
        this.timeInStatus = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}