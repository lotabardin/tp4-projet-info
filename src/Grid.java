import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Grid {
    private List<Agent>[][] cells;
    private int width, height;

    /**
     * Constructor for the Grid class
     * @param width The width of the grid
     * @param height The height of the grid
     */
    @SuppressWarnings("unchecked")
    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        this.cells = new ArrayList[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.cells[i][j] = new ArrayList<>();
            }
        }
    }

    /**
     * Add an agent to the grid
     * @param agent The agent to add
     * @param x The x-coordinate of the cell
     * @param y The y-coordinate of the cell
     */
    public void addAgent(Agent agent, int x, int y) {
        cells[x][y].add(agent);
    }

    /**
     * Move an agent from one cell to another
     * @param agent The agent to remove
     * @param x The x-coordinate of the cell
     * @param y The y-coordinate of the cell
     */
    public void moveAgent(Agent agent, int oldX, int oldY, int newX, int newY) {
        cells[oldX][oldY].remove(agent);
        cells[newX][newY].add(agent);
    }

    /**
     * Get the list of agents in the vicinity of a given cell
     * @param x The x-coordinate of the cell
     * @param y The y-coordinate of the cell
     * @return A list of agents in the vicinity
     */
    public List<Agent> getAgentsInVicinity(int x, int y) {
        List<Agent> nearbyAgents = new ArrayList<>();
        int[] dx = {-1, 0, 1, -1, 1, -1, 0, 1};
        int[] dy = {-1, -1, -1, 0, 0, 1, 1, 1};

        for (int i = 0; i < dx.length; i++) {
            int nx = (x + dx[i] + width) % width;
            int ny = (y + dy[i] + height) % height;

            nearbyAgents.addAll(cells[nx][ny]);
        }
        nearbyAgents.addAll(cells[x][y]);

        return nearbyAgents;
    }

    /**
     * Place an agent randomly on the grid
     * @param agent The agent to place
     * @param randomGenerator The random number generator
     */
    public void placeAgentRandomly(Agent agent, Random randomGenerator) {
        int x = randomGenerator.nextInt(width);
        int y = randomGenerator.nextInt(height);
        addAgent(agent, x, y);
    }

    /**
     * Print the grid
     */
    public void printBoard(){
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print(cells[i][j].size() + " ");
                // print the status of the agents
                for (Agent agent : cells[i][j]) {
                    System.out.print(agent.getStatus() + " ");
                }
            }
            System.out.println();
        }
    }

}