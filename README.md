# Projet : Modèle Toy case SEIR multi-agent 
## Simulation de Propagation d'une Maladie dans une Population

UCA Clermont-Ferrand - Auvergne

### Nom des auteurs:
- Tabardin Loïc
- Doynuk Semih
- Jouhannet Tristan

### Date de rendu: 
- 07/04/2024

### Cours: 
- Projet Info

## Structure du Projet

Ce projet contient plusieurs dossier :

- `src/`: Contient les fichiers source Java pour la simulation dont :
    - `Agent.java`: Définit les propriétés et le comportement des agents dans la simulation.
    - `Grid.java`: Représente l'espace de la simulation où les agents se déplacent et interagissent.
    - `Simulation.java`: Orchestre la simulation, gérant les mises à jour des agents et l'enregistrement des résultats.
    - `Main.java`: Le point d'entrée de la simulation, exécutant le modèle avec des paramètres prédéfinis.

- `csv/`: Contient les fichiers CSV générés par la simulation, stockant les résultats de chaque simulation.

- `Jupyter_TP4.ipynb`: Un notebook Jupyter pour visualiser et analyser les résultats de la simulation.

- `TP4.pdf`: Le sujet du projet avec les consignes.

- `rapport Projet.pdf`: Le rapport du projet avec les résultats et les analyses.

- `README.md`: Ce fichier.

## Comment Exécuter

Pour exécuter la simulation, assurez-vous que vous avez Java installé sur votre système. Compilez les fichiers source et exécutez la classe Main :

javac Main.java
java Main

## Résultat

Les résultats de la simulation sont enregistrés dans des fichiers CSV sous le répertoire ./csv/. Chaque simulation génère un fichier distinct nommé Test i.csv, où i est le numéro de la simulation.

## Visualisation

Un Jupyter Notebook Analysis.ipynb est inclus pour visualiser et analyser les résultats de la simulation. Il nécessite une installation de Python avec les bibliothèques pandas, matplotlib et seaborn.